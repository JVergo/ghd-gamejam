﻿using UnityEngine;
using System.Collections;
using Rewired;

/*
The Script is structured the following way:
[Public & Private Fields]
[Unity Monobehaviour Method calls (start(), Update() etc.)]
[Event Methods]
[Coroutine Methods]
[Normal Methods]
*/

/// <summary>
/// The Player Controller. Takes care of the Player Movement, Camera, Input and some Animations.
/// </summary>
[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Player_Info))]
[RequireComponent(typeof(Animator))]
public class Player_Controller : MonoBehaviour
{
    #region Uncatagoriseable fields
    [Header("Uncatagoriseable")]
    [Tooltip("The Distance from PlayerCam that the player can Interact.")]
    [SerializeField]
    float _InteractDistance = 1.0f;
    Player _player; //The Player from Rewired input
    CharacterController _charControl; //The Gameobject CharacterController. Affects movement, camera height, collisions.
    Player_Info _playerInfo; //Player info script
    public Animator animator;//Animator attached
    #endregion

    #region Movement and Movement Modifiers fields
    [Header("Movement and Movement Modifiers")]
    [Tooltip("How fast the player is moving when standing.")]
    [SerializeField]
    float baseMovementSpeed = 3f;
    [Tooltip("How fast the player is moving when crouching. This is pct of baseMovementSpeed.")]
    [Range(0f, 1)]
    [SerializeField]
    float _CrouchMovement = 0.50f;
    [Tooltip("How fast the player is moving when crawling. This is pct of baseMovementSpeed.")]
    [Range(0f, 1)]
    [SerializeField]
    float _CrawlMovementModifier = 0.30f;
    float _currentMovementSpeed; // The current speed the player is moving at
    [Tooltip("How fast the the speed change is applied when sprinting. Higher = faster eks. 2 makes it take ~0.5 seconds.")]
    [SerializeField]
    float _SpeedChangeSpeed = 2;
    float _targetMovementModifier = 1;//Current target for the movement modifier
    float _currentMovementSpeedModifier = 1.0f; //the current pct of baseMovementSpeed that is multiplied to movement
    [Tooltip("Increase in how many times faster the player moves when sprinting. CurrentMovementModifier = BaseMovementModifier + _SprintMovementModifier")]
    [Range(0f, 10)]
    [SerializeField]
    float _SprintMovementModifier = 2.5f;
    Vector3 _movementDirection; //Vector for holding movement direction input
    bool _isSprinting = false;//Toggle for movement modifiers.
    #endregion

    #region Camera & Rotation fields
    [Header("Camera & Rotation")]
    [Tooltip("This is the Camera used by the player.")]
    [SerializeField]
    Camera playerCam;
    [Tooltip("The height of the camera. This is pct of its height. If this is 0.8 and player is 2 meters height then the camera is placed 1.6 meters above the feet.")]
    [Range(0.0f, 1.0f)]
    [SerializeField]
    float cameraHeight = 1.0f;
    [Tooltip("(Up/Down Rotation). The Maximum difference in degree that the X-Axis may differ from Quaternion.identity.X .")]
    [SerializeField]
    float xClampingDistance = 60;
    [Tooltip("(Left/Right Rotation). The Maximum distance on Y-Axis that may be between playerCam forward & this.Gameobject forward.")]
    [SerializeField]
    float maxYRotationDifference = 80;
    [Tooltip("How fast this.Gameobject.Transform.rotation rotates to this.playerCam.Transform.rotation when camera lerping.")]
    [SerializeField]
    float characterRotationSpeed = 5;
    [Tooltip("The Multiplier that is applied to characterRotationSpeed in the camera lerping when moving. To make it respond faster in the correct direction.")]
    [SerializeField]
    float turnSpeedOnMovementMultiplier = 5;
    bool _lerpInProgress = false; //used to check whether there Is a lerp on this.rotation occuring.
    Vector3 _rotateDirection; //Used for storing values for camera rotation Camera Input Axis Movement
    [Tooltip("How far the camera will moves up and down from camera start position when moving.")]
    [Range(0.00f, 1)]
    [SerializeField]
    float headBobYStrength = 0.05f;
    [Tooltip("How fast the camera will moves up and down when moving.")]
    [Range(0.00f, 20)]
    [SerializeField]
    float headBobYFrequency = 12;
    [Tooltip("How far the camera will moves left and right from camera start position when moving.")]
    [Range(0.00f, 1)]
    [SerializeField]
    float headBobXStrength = 0.1f;
    [Tooltip("How fast the camera will moves left and right when moving forward or backward. Recommended is half of headBobYFrequency.")]
    [Range(0.00f, 20)]
    [SerializeField]
    float headBobXFrequency = 6f;
    Vector3 _sway;//The Current Sidesway
    [Tooltip("How far the camera will moves left and right when moving sideways.")]
    [SerializeField]
    float headSwayStrengh = 0.05f;
    [Tooltip("How fast the camera will moves left and right when moving sideways. eg 2 is equal to twice as fast as normal")]
    [SerializeField]
    float headSwaySpeed = 2f;
    bool cameraLock = false; //whether the camera is locked or not
                             //See Also Player Selectable Options for: mouseSensitivity
    #endregion

    #region Player character height fields
    [Header("Player character height")]
    [Tooltip("The Height of the player when standing. This should match the standing height of the model used.")]
    public float standingPlayerHeight = 2.0f;
    [Tooltip("The Height of the player when crouching. This is a pct of the standingPlayerHeight.")]
    [Range(0f, 1f)]
    public float crouchingPlayerHeight = 0.5f;
    [Tooltip("The Height of the player when crawling. This is a pct of the standingPlayerHeight.")]
    [Range(0f, 1f)]
    public float crawlingPlayerHeight = 0.25f;
    [Tooltip("This is how fast the height of the player changes when changing state (like from Standing -> Crawling).")]
    public float heightChangeSpeed = 3.0f;
    enum CharacterState { standing, crouching, crawling } //States of player. Crouching & crawling is also used when checking for toggle on/off See their respected Event Methods
    CharacterState _currentState; //States of player.
    #endregion

    #region Jump and gravity fields
    [Header("Jump and gravity")]
    [Tooltip("The Force applied to the character when falling.")]
    public float gravity = 20f;
    [Tooltip("The Force applied to the character when pressing the jump button.")]
    public float jumpForce = 80;
    float _currrentYDirectionForce = 0.0f;
    #endregion

    #region Player Selectable Options fields
    [Header("Player Selectable Options")]
    [Tooltip("If true: Sets Sprint button to be Toggle.")]
    public bool isSprintToggle = false;
    [Tooltip("If true: Sets Crouch button to be Toggle.")]
    public bool isCrouchToggle = false;
    [Tooltip("If true: Sets Crawl button to be Toggle.")]
    public bool isCrawlToggle = false;
    [Tooltip("The Sensitivity of the Mouse.")]
    public float mouseSensitivity = 2.0f;
    #endregion

    GameObject interactprompt;
    GameObject menuPanale;

    void Awake()
    {
        //Gets the components
        animator = GetComponent<Animator>();
        this._playerInfo = gameObject.GetComponent<Player_Info>();
        this._charControl = this.GetComponent<CharacterController>();

        //Gets the Player from ReWired
        this._player = Rewired.ReInput.players.GetPlayer(0);

        //Creates events for movement
        this._player.AddInputEventDelegate(OnForwardInputMovement, UpdateLoopType.Update, "Forward movement"); //movement forwards and backwards
        this._player.AddInputEventDelegate(OnSidewaysInputMovement, UpdateLoopType.Update, "Sideways movement"); //movement left and right

        //Triggers the player jump
        this._player.AddInputEventDelegate(Jump, UpdateLoopType.Update, InputActionEventType.ButtonJustPressed, "Jump");

        //Turns Sprint on/off (Toggle if option is checked)
        this._player.AddInputEventDelegate(Sprint, UpdateLoopType.Update, InputActionEventType.ButtonJustPressed, "ToggleSprint");
        this._player.AddInputEventDelegate(Sprint, UpdateLoopType.Update, InputActionEventType.ButtonJustReleased, "ToggleSprint");

        //Turns Crouch on/off (Toggle if option is checked)
        this._player.AddInputEventDelegate(Crouch, UpdateLoopType.Update, InputActionEventType.ButtonJustPressed, "ToggleCrouch");
        this._player.AddInputEventDelegate(Crouch, UpdateLoopType.Update, InputActionEventType.ButtonJustReleased, "ToggleCrouch");

        //Turns crawl on/off (Toggle if option is checked)
        this._player.AddInputEventDelegate(Crawl, UpdateLoopType.Update, InputActionEventType.ButtonJustPressed, "ToggleCrawl");
        this._player.AddInputEventDelegate(Crawl, UpdateLoopType.Update, InputActionEventType.ButtonJustReleased, "ToggleCrawl");

        //Creates events for Camera movement
        this._player.AddInputEventDelegate(OnHorizontalCameraInputMovement, UpdateLoopType.Update, InputActionEventType.AxisActive, "Horizontal Cam");//Camera movement Left and Right
        this._player.AddInputEventDelegate(OnVerticalCameraInputMovement, UpdateLoopType.Update, InputActionEventType.AxisActive, "Vertical Cam");//Camera movement Up and Down
        
        this._player.AddInputEventDelegate(OpenInventory, UpdateLoopType.Update, InputActionEventType.ButtonJustPressed, "Open Inventory");//Event for opening inventory 
        this._player.AddInputEventDelegate(Interact, UpdateLoopType.Update, InputActionEventType.ButtonJustPressed, "Interact");//Event for Interacting

        //Events for using weapons Primary, Secondary and Tertiary action of weapon
        this._player.AddInputEventDelegate(WeaponPrimary, UpdateLoopType.Update, InputActionEventType.Update, "Weapon Primary");
    }

    void Start()
    {
        //Sets some fields values
        this._movementDirection = new Vector3();
        this._rotateDirection = new Vector3();
        _currentMovementSpeed = baseMovementSpeed;
        this._currentState = CharacterState.standing;

        //Sets Camera position to player position and Camera rotation to player rotation.
        this.playerCam.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + (this._charControl.height * cameraHeight), this.transform.position.z);
        this.playerCam.transform.rotation = this.transform.rotation;

        //Its an unpause method... Yeah i know the name isn't good and can be misunderstood for enabling free cam, just didn't have a better name and still don't have one... basically Brain fart.
        
        interactprompt = GameObject.Find("Interactpormpt");
        menuPanale = GameObject.Find("MenuPanale");
        menuPanale.SetActive(false);
        LockCamera(false);
    }

    void Update()
    {
        if (!cameraLock)
        {
            CameraLook();
            ExecuteMovement();
            UpdateAnimation();
        }

        //if (Input.GetKeyDown(KeyCode.Escape))//Temp Key to exit build game 
        //{
        //    Application.Quit();
        //}

        if (Input.GetKeyDown(KeyCode.Escape))//Temp Key Lock and unlock camara and mouse
        {
            if (cameraLock)
            {
                LockCamera(false);
                menuPanale.SetActive(false);
            }
            else if (!cameraLock)
            {
                LockCamera(true);
                menuPanale.SetActive(true);
            }
        }

    }

    void FixedUpdate()
    {
        RaycastHit interactable;
        IInteractable t;

        Debug.DrawRay(this.playerCam.transform.position, this.playerCam.transform.TransformDirection(Vector3.forward) * _InteractDistance);

        if (Physics.Raycast(this.playerCam.transform.position, this.playerCam.transform.TransformDirection(Vector3.forward), out interactable, _InteractDistance))
        {
            if ((t = interactable.collider.gameObject.GetComponentInParent<IInteractable>()) != null)
            {
                interactprompt.SetActive(true);
            }
            else
            {
                if (interactprompt.activeSelf)
                {
                    interactprompt.SetActive(false);
                }
            }
        }
        else
        {
            interactprompt.SetActive(false);
        }
    }

    /// <summary>
    /// Event: Gets input information for (negative or positive) forward movement from the <see cref="InputActionEventData"/> and stores it into <see cref="_movementDirection"/> field.
    /// </summary>
    /// <param name="data"></param>
    void OnForwardInputMovement(InputActionEventData data)
    {
        if (_charControl.isGrounded)
            this._movementDirection.z = data.GetAxis();
    }

    /// <summary>
    /// Event: Gets input information for sideways movement from the <see cref="InputActionEventData"/> and stores it into <see cref="_movementDirection"/> field.
    /// </summary>
    /// <param name="data"></param>
    void OnSidewaysInputMovement(InputActionEventData data)
    {
        if (_charControl.isGrounded)
            this._movementDirection.x = data.GetAxis();
    }

    /// <summary>
    /// Event: activate Sprint for the Player by starting a <see cref="Coroutine"/> named <see cref="LerpSpeedmodifierChange(float)"/> with the speed change <see cref="_SprintMovementModifier"/>: Negative if off or positive if on.
    /// </summary>
    /// <param name="data"></param>
    void Sprint(InputActionEventData data)
    {
        if (isSprintToggle)
        {
            if (!data.GetButtonPrev())
            {
                if (_isSprinting)
                {
                    _isSprinting = false;
                    StartCoroutine(LerpSpeedmodifierChange(-_SprintMovementModifier));
                }
                else
                {
                    _isSprinting = true;
                    StartCoroutine(LerpSpeedmodifierChange(_SprintMovementModifier));
                }
            }
        }
        else
        {
            if (data.GetButtonPrev())
            {
                _isSprinting = false;
                StartCoroutine(LerpSpeedmodifierChange(-_SprintMovementModifier));
            }
            else
            {
                _isSprinting = true;
                StartCoroutine(LerpSpeedmodifierChange(_SprintMovementModifier));
            }
        }

        //if (_isSprinting)//temp for current animation
        //{
        //    animator.SetBool("IdleReady", true);
        //    animator.SetBool("IdleStand", false);

        //    animator.SetBool("IdleWalk", false);
        //    animator.SetBool("WalkBackward", false);
        //    animator.SetBool("IdleStrafeRight", false);
        //    animator.SetBool("IdleStrafeLeft", false);
        //}
        //else
        //{
        //    animator.SetBool("IdleStand", true);
        //    animator.SetBool("IdleReady", false);

        //    animator.SetBool("IdleRun", false);
        //    animator.SetBool("RunBackward", false);
        //    animator.SetBool("StrafeRunRight", false);
        //    animator.SetBool("StrafeRunLeft", false);
        //}
    }

    /// <summary>
    /// Event: activate Crouch for the Player by starting a <see cref="Coroutine"/> named <see cref="ApplyCharacterStateChange(float, float, CharacterState)"/> with the % speed change of <see cref="baseMovementSpeed"/>, Pct height change of <see cref="standingPlayerHeight"/> and the name of the new state
    /// </summary>
    /// <param name="data"></param>
    void Crouch(InputActionEventData data)
    {
        if (isCrouchToggle)
        {
            if (!data.GetButtonPrev())
            {
                if (_currentState == CharacterState.crouching && _currentState != CharacterState.crawling)
                {
                    StartCoroutine(ApplyCharacterStateChange(1, 1, CharacterState.standing));
                }
                else
                {
                    StartCoroutine(ApplyCharacterStateChange(_CrouchMovement, crouchingPlayerHeight, CharacterState.crouching));
                }
            }
        }
        else
        {
            if (data.GetButtonPrev() && _currentState != CharacterState.crawling)//Checks whether the button was down or up in the previous frame. This is if it is Up.
            {
                StartCoroutine(ApplyCharacterStateChange(1, 1, CharacterState.standing));
            }
            else if (!data.GetButtonPrev())//This is if it is Down.
            {
                StartCoroutine(ApplyCharacterStateChange(_CrouchMovement, crouchingPlayerHeight, CharacterState.crouching));
            }
        }

    }

    /// <summary>
    /// Event: activate Crawl for the Player by starting a <see cref="Coroutine"/> named <see cref="ApplyCharacterStateChange(float, float, CharacterState)"/> with the % speed change of <see cref="baseMovementSpeed"/>, Pct height change of <see cref="standingPlayerHeight"/> and the name of the new state
    /// </summary>
    /// <param name="data"></param>
    void Crawl(InputActionEventData data)
    {
        if (isCrawlToggle)
        {
            if (!data.GetButtonPrev())
            {
                if (_currentState == CharacterState.crawling && _currentState != CharacterState.crouching)
                {
                    StartCoroutine(ApplyCharacterStateChange(1, 1, CharacterState.standing));
                }
                else
                {
                    StartCoroutine(ApplyCharacterStateChange(_CrawlMovementModifier, crawlingPlayerHeight, CharacterState.crawling));
                }
            }
        }
        else
        {
            if (data.GetButtonPrev() && _currentState != CharacterState.crouching)//Checks whether the button was down or up in the previous frame. This is if it is Up.
            {
                StartCoroutine(ApplyCharacterStateChange(1, 1, CharacterState.standing));
            }
            else if (!data.GetButtonPrev()) //This is if it is Down.
            {
                StartCoroutine(ApplyCharacterStateChange(_CrawlMovementModifier, crawlingPlayerHeight, CharacterState.crawling));
            }
        }
    }

    /// <summary>
    /// Event: Sets the <see cref="_movementDirection"/> to <see cref="jumpForce"/> for jump.
    /// </summary>
    /// <param name="data"></param>
    void Jump(InputActionEventData data)
    {
        if (this._charControl.isGrounded && _currentState == CharacterState.standing)
            _currrentYDirectionForce = jumpForce;
    }

    /// <summary>
    /// Event: Gets input information for Horizontal camera rotation from the <see cref="InputActionEventData"/> and stores it into <see cref="_rotateDirection"/>.
    /// </summary>
    /// <param name="data"></param>
    void OnHorizontalCameraInputMovement(InputActionEventData data)
    {
        _rotateDirection.y = data.GetAxis() * mouseSensitivity * Time.deltaTime;
    }

    /// <summary>
    /// Event: Gets input information for Vertical camera rotation from the <see cref="InputActionEventData"/> and stores it into <see cref="_rotateDirection"/>.
    /// </summary>
    /// <param name="data"></param>
    void OnVerticalCameraInputMovement(InputActionEventData data)
    {
        _rotateDirection.x = -data.GetAxis() * mouseSensitivity * Time.deltaTime;
    }

    /// <summary>
    /// Event: [Incomplete] Opens the player Inventory
    /// </summary>
    /// <param name="data"></param>
    void OpenInventory(InputActionEventData data)
    {

    }

    /// <summary>
    /// Event: Makes a <see cref="Ray"/> from this postion in the relative forward position to interact with target object, if it implements <see cref="IInteractable"/>
    /// </summary>
    /// <param name="data"></param>
    void Interact(InputActionEventData data)
    {
        RaycastHit interactable;
        IInteractable t;

        Debug.DrawRay(this.playerCam.transform.position, this.playerCam.transform.TransformDirection(Vector3.forward) * _InteractDistance);

        if (Physics.Raycast(this.playerCam.transform.position, this.playerCam.transform.TransformDirection(Vector3.forward), out interactable, _InteractDistance))
        {
            if ((t = interactable.collider.gameObject.GetComponentInParent<IInteractable>()) != null)
                t.Interact(true);
        }
    }

    /// <summary>
    /// Event: calls <see cref="Player_Info._activeWeapon"/> <see cref="Weapon"/> Primary Actions, if there is none then it calls the <see cref="Player_Info.MeleeAttack"/> method
    /// </summary>
    /// <param name="data"></param>
    void WeaponPrimary(InputActionEventData data)
    {
        if (cameraLock)
            return;

        if (data.GetButtonDown())
        {
            //Code goes here for when button is Just pressed
            if (Mathf.DeltaAngle(this.transform.rotation.eulerAngles.y, this.playerCam.transform.rotation.eulerAngles.y) != 0)
            {
                StartCoroutine(LerpPlayerYToCamY(10f));
            }
            if (this._playerInfo.weapon != null)
            {
                this._playerInfo.weapon.Fire();
            }
            else
            {

            }
        }
    }

    /// <summary>
    /// Coroutine Method for lerp of <see cref="_charControl.height"/> and <see cref="_currentMovementSpeed"/> by pct relative to <see cref="standingPlayerHeight"/> and <see cref="standingPlayerHeight"/> respectfully 
    /// </summary>
    /// <param name="pctSpeedChange"></param>
    /// <param name="pctHeightChange"></param>
    /// <returns></returns>
    IEnumerator ApplyCharacterStateChange(float pctSpeedChange, float pctHeightChange, CharacterState state)
    {
        _currentState = state;

        CharacterState localState = state; //Used to check if there has been a Change in Character state (Player Crounches -> Player realize they need to crawl -> Player crawl -> this crouch Routine runs and see that a change has occurred and ends)

        float previousPlayerHeight = this._charControl.height;
        float previousPlayerSpeed = this._currentMovementSpeed;
        Vector3 previousPlayerCenter = this._charControl.center;

        float newSpeed = this.baseMovementSpeed * pctSpeedChange;
        float newHeight = this.standingPlayerHeight * pctHeightChange;
        Vector3 newCenter = new Vector3(this._charControl.center.x, (standingPlayerHeight * 0.5f) * pctHeightChange, this._charControl.center.z);

        float time = 0;

        while (time < 1)
        {
            if (_currentState != localState)
            {
                yield break;
            }

            if (previousPlayerHeight < newHeight)//The content of this if sentence is to prevent standing up into things. Apply only when the Character is trying to stand up
            {
                Vector3 p1 = transform.position; //Buttom point of Capsule 
                Vector3 p2 = p1 + (Vector3.up * (_charControl.height - 0.2f)); //Top point of Capsule 

                if (!Physics.CapsuleCast(p1, p2, _charControl.radius, Vector3.up, 0.2f, LayerMask.GetMask("Default")))
                {
                    time += Time.deltaTime * heightChangeSpeed;
                }
            }
            else
                time += Time.deltaTime * heightChangeSpeed;

            this._charControl.height = Mathf.Lerp(previousPlayerHeight, newHeight, time);
            //this._charControl.center = Vector3.Lerp(previousPlayerCenter, newCenter, time);
            this._currentMovementSpeed = Mathf.Lerp(previousPlayerSpeed, newSpeed, time);

            yield return null;
        }
    }

    /// <summary>
    /// Coroutine Method for lerp of <see cref="_currentMovementSpeedModifier"/> to <see cref="_targetMovementModifier"/> by <see cref="_SpeedChangeSpeed"/>
    /// </summary>
    /// <param name="spreedMovementModifierChange"></param>
    /// <returns></returns>
    IEnumerator LerpSpeedmodifierChange(float spreedMovementModifierChange)
    {
        float previousPlayerSpeedModifier = this._currentMovementSpeedModifier;
        _targetMovementModifier += spreedMovementModifierChange;
        float time = 0;

        if (!_isSprinting)//Deaccelerating
        {
            while (_currentMovementSpeedModifier != _targetMovementModifier)
            {

                if (_charControl.isGrounded)
                {
                    time += Time.deltaTime * _SpeedChangeSpeed;
                }
                if (_charControl.velocity == Vector3.zero)
                {
                    _currentMovementSpeedModifier = _targetMovementModifier;
                    yield break;
                }

                this._currentMovementSpeedModifier = Mathf.Lerp(previousPlayerSpeedModifier, _targetMovementModifier, time);

                yield return null;
            }
        }
        else //accelerating
            while (_isSprinting)
            {
                if (_charControl.isGrounded)
                {
                    time += Time.deltaTime * _SpeedChangeSpeed;
                }

                if (_charControl.velocity == Vector3.zero)
                {
                    time = 0;
                }

                this._currentMovementSpeedModifier = Mathf.Lerp(previousPlayerSpeedModifier, _targetMovementModifier, time);

                yield return null;
            }

    }

    /// <summary>
    /// <see cref="Coroutine"/> Method for slerping between <see cref="this.transform.rotation.y"/> and <see cref="this.playerCam.transform.rotation.y"/>
    /// </summary>
    /// <returns></returns>
    IEnumerator LerpPlayerYToCamY(float speedMultiplier)
    {
        this._lerpInProgress = true;
        float time = 0;
        float playerCamYRotation;
        Quaternion targetRotation = Quaternion.identity;

        while (time < 1 || Mathf.DeltaAngle(this.transform.rotation.eulerAngles.y, this.playerCam.transform.rotation.eulerAngles.y) > 5)
        //while(Mathf.DeltaAngle(this.transform.rotation.eulerAngles.y, this.playerCam.transform.rotation.eulerAngles.y) != 0)
        {
            if (!_charControl.isGrounded)
            {
                this._lerpInProgress = false;
                yield break;
            }

            playerCamYRotation = this.playerCam.transform.rotation.eulerAngles.y;
            targetRotation.eulerAngles = new Vector3(0, playerCamYRotation, 0);

            time += this.characterRotationSpeed * speedMultiplier * Time.deltaTime;

            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, targetRotation, time);

            targetRotation = Quaternion.identity;
            yield return null;
        }
        this._lerpInProgress = false;
    }

    /// <summary>
    /// Moves the <see cref="_charControl"/>(the Player) in the direction the Player is looking in also updates the <see cref="playerCam"/> position
    /// </summary>
    void ExecuteMovement()
    {
        _movementDirection.Normalize(); //makes the direction size of 1

        Vector3 relativeDirection = this.transform.TransformDirection(_movementDirection);//get the direction relative to the player characters rotation

        relativeDirection *= this._currentMovementSpeed * this._currentMovementSpeedModifier * (_movementDirection.z < 0 ? 0.5f : 1);//adds speed onto the relativeDirection and if player is walking backwards: reduce their speed by 50%

        if (!_charControl.isGrounded)
        {
            //Apply Gravity.
            _currrentYDirectionForce -= gravity * Time.deltaTime;
        }

        //Apply upwardforce (or zero it) to the current Move.
        relativeDirection.y = _currrentYDirectionForce;

        //Moves the character
        this._charControl.Move(relativeDirection * Time.deltaTime);

        //Slerp character to the camera if there is movement (and it aren't by jumping). And stop the current Interaction.
        if (_charControl.velocity != Vector3.zero && _charControl.isGrounded)
        {
            if (!_lerpInProgress)
            {
                StartCoroutine(LerpPlayerYToCamY(turnSpeedOnMovementMultiplier));
            }
        }

        //Updates the camera position and applies headbob
        this.playerCam.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + (this._charControl.height * cameraHeight), this.transform.position.z) + Headbob() + HeadSway() + playerCam.transform.TransformDirection(Vector3.forward * 0.1f); //Last vector is temp... unless you wish to see eyes
    }

    /// <summary>
    /// Method for rotation the <see cref="playerCam"/> according to <see cref="_rotateDirection"/> which gets its values from <see cref="OnHorizontalCameraInputMovement(InputActionEventData)"/> & <see cref="OnVerticalCameraInputMovement(InputActionEventData)"/>.
    /// also Clams the rotation on the x axis (up and down) according to <see cref="xClampingDistance"/>
    /// </summary>
    /// <remarks>
    /// The standard <see cref="this.transform.rotation.x"/> 0 degree is not directly up nor directly down but in the middle which makes things a bit wierd when it comes to camera clamping. A rewrite with a renewed look into the workings of Quaternion might be a good idea.
    /// </remarks>
    void CameraLook()
    {
        Quaternion newRotation = Quaternion.Euler(this.playerCam.transform.rotation.eulerAngles + _rotateDirection); //The new Rotation 

        //Camera xClamp       
        if (Quaternion.Angle(Quaternion.identity, Quaternion.Euler(playerCam.transform.rotation.eulerAngles.x + _rotateDirection.x, 0, 0)) > xClampingDistance)
        {
            _rotateDirection.x = 0;
            newRotation = Quaternion.Euler(this.playerCam.transform.rotation.eulerAngles + _rotateDirection); //The new Rotation
        }
        //--------------------------------------------------------------------------------------------

        if (_charControl.isGrounded)
        {
            this.playerCam.transform.rotation = newRotation;
        }
        else
        {
            if (!(Quaternion.Angle(Quaternion.Euler(0, this.transform.rotation.eulerAngles.y, 0), Quaternion.Euler(0, playerCam.transform.rotation.eulerAngles.y, 0)) > maxYRotationDifference)) //Makes sure you can't look more than maxYRotationDifference when you are in the air
            {
                this.playerCam.transform.rotation = newRotation;
            }
        }

        _rotateDirection.Set(0, 0, 0);

        //Check if the difference between players y rotation and cameras y rotation is greater than maxYRotationDifference
        if (Quaternion.Angle(Quaternion.Euler(0, this.transform.rotation.eulerAngles.y, 0), Quaternion.Euler(0, playerCam.transform.rotation.eulerAngles.y, 0)) > maxYRotationDifference && _charControl.isGrounded)
        {
            if (!_lerpInProgress)
            {
                StartCoroutine(LerpPlayerYToCamY(1));
            }
        }

    }

    /// <summary>
    /// Method that applies headbob to the <see cref="playerCam"/> when <see cref="_charControl"/> is moving forward/backward depending on speed and direction
    /// </summary>
    /// <returns></returns>
    Vector3 Headbob()
    {
        Vector3 headbob = Vector3.zero;

        if (_charControl.isGrounded)
        {
            headbob = this.transform.TransformDirection(new Vector3(
            Mathf.Sin(headBobXFrequency * _player.GetAxisTimeActive("Forward movement")) * headBobXStrength * (_currentMovementSpeedModifier * _currentMovementSpeed * 0.2f),
            Mathf.Sin(headBobYFrequency * _player.GetAxisTimeActive("Forward movement")) * headBobYStrength * (_currentMovementSpeedModifier * _currentMovementSpeed * 0.2f),
            0)) * (_movementDirection.z < 0 ? 0.5f : 1);
        }

        return headbob;
    }

    /// <summary>
    /// Method that applies headsway to the <see cref="playerCam"/> when <see cref="_charControl"/> is moving sideways depending on speed and direction
    /// </summary>
    /// <returns></returns>
    Vector3 HeadSway()
    {
        if (_player.GetAxisTimeActive("Sideways movement") > 0)
        {
            _sway = this.transform.TransformDirection(new Vector3(Mathf.Lerp(0f, headSwayStrengh, _player.GetAxisTimeActive("Sideways movement") * headSwaySpeed) * _movementDirection.x, 0, 0));
        }
        else if (_sway != Vector3.zero)
        {
            _sway = this.transform.TransformDirection(new Vector3(Mathf.Lerp(this.transform.InverseTransformDirection(_sway).x, 0, _player.GetAxisRawTimeInactive("Sideways movement") * headSwaySpeed), 0, 0));
        }

        return _sway;
    }

    /// <summary>
    /// Locks the Camera, unlock and show mouse
    /// </summary>
    public void LockCamera(bool islocked)
    {
        if (islocked)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        cameraLock = islocked;
    }

    /// <summary>
    /// Temp Method for activating the Temp Player Animations
    /// </summary>
    private void UpdateAnimation()
    {
        //if (_movementDirection.z > 0)
        //{
        //    if (_isSprinting)
        //    {
        //        animator.SetBool("IdleRun", true);
        //        animator.SetBool("RunBackward", false);
        //    }
        //    else
        //    {
        //        animator.SetBool("IdleWalk", true);
        //        animator.SetBool("WalkBackward", false);
        //    }
        //}
        //else if (_movementDirection.z < 0)
        //{
        //    if (_isSprinting)
        //    {
        //        animator.SetBool("RunBackward", true);
        //        animator.SetBool("IdleRun", false);
        //    }
        //    else
        //    {
        //        animator.SetBool("WalkBackward", true);
        //        animator.SetBool("IdleWalk", false);
        //    }
        //}
        //else
        //{
        //    if (_isSprinting)
        //    {
        //        animator.SetBool("IdleRun", false);
        //        animator.SetBool("RunBackward", false);
        //    }
        //    else
        //    {
        //        animator.SetBool("IdleWalk", false);
        //        animator.SetBool("WalkBackward", false);
        //    }
        //}

        //if (_movementDirection.x > 0)
        //{
        //    if (_isSprinting)
        //    {
        //        animator.SetBool("StrafeRunRight", true);
        //        animator.SetBool("StrafeRunLeft", false);
        //    }
        //    else
        //    {
        //        animator.SetBool("IdleStrafeRight", true);
        //        animator.SetBool("IdleStrafeLeft", false);
        //    }
        //}
        //else if (_movementDirection.x < 0)
        //{
        //    if (_isSprinting)
        //    {
        //        animator.SetBool("StrafeRunLeft", true);
        //        animator.SetBool("StrafeRunRight", false);
        //    }
        //    else
        //    {
        //        animator.SetBool("IdleStrafeLeft", true);
        //        animator.SetBool("IdleStrafeRight", false);
        //    }
        //}
        //else
        //{
        //    if (_isSprinting)
        //    {
        //        animator.SetBool("StrafeRunRight", false);
        //        animator.SetBool("StrafeRunLeft", false);
        //    }
        //    else
        //    {
        //        animator.SetBool("IdleStrafeRight", false);
        //        animator.SetBool("IdleStrafeLeft", false);
        //    }
        //}
    }
}