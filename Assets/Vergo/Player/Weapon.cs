﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Weapon : MonoBehaviour
{
    public AudioSource gunSound;
    public AudioClip shoot, reload;
    [SerializeField]
    private GameObject _ammo;
    [SerializeField]
    private Transform spawnPoint;
    [SerializeField]
    private float _fireRate;
    private float _fireTimer;
    private int ammoCount;
    public int AmmoCount
    {
        get { return ammoCount; }
        set
        {
            ammoCount = value;
            bulletCountFelad.text = "Bullets in gun: " + clipCount + " / 6.\nBullets left: " + ammoCount;
        }
    }
    [SerializeField]
    private int clipCount;
    private int ClipCount
    {
        get { return clipCount; }
        set
        {
            clipCount = value;
            bulletCountFelad.text = "Bullets in gun: " + clipCount + " / 6.\nBullets left: " + ammoCount;
        }
    }
    private bool reloading;
    private float reloadTimer;

    public Text bulletCountFelad;

    public void Start()
    {
        spawnPoint = this.transform.FindChild("SpawnPoint");
        gunSound = this.GetComponent<AudioSource>();
        ClipCount = 6;
        reloading = false;
    }

    public void Fire()
    {
        if (_fireTimer <= 0)
        {
            if (ClipCount > 0)
            {
                gunSound.PlayOneShot(shoot);
                GameObject shot = Instantiate(_ammo, spawnPoint.transform.position, Camera.main.transform.rotation) as GameObject;
                shot.GetComponent<Rigidbody>().AddForce(spawnPoint.transform.forward * 1500);
                Destroy(shot.GetComponent<Bullet>());
                shot.transform.localScale = Vector3.one;
                shot.name = "Bullet";
                Destroy(shot, 5);
                _fireTimer = _fireRate;
                ClipCount--;
                if (ClipCount == 0)
                {
                    reloading = true;
                }
            }
            else
            {
                reloading = true;
            }
        }
    }

    void Update()
    {
        if (_fireTimer > 0)
        {
            _fireTimer -= Time.deltaTime;
        }

        if (reloading && AmmoCount > 0)
        {
            reloadTimer += Time.deltaTime;
            if (reloadTimer >= 1)
            {
                AmmoCount--;
                ClipCount++;
                reloadTimer -= 1;
                gunSound.PlayOneShot(reload);
                if (ClipCount == 6)
                {
                    reloading = false;
                }
            }
        }
    }

    public void BeginReload()
    {
        reloading = true;
    }
}
