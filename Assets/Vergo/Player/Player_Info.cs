﻿using UnityEngine;
using System.Collections;

public class Player_Info : MonoBehaviour
{
    public Weapon weapon;
    private int keys;
    public int Keys
    {
        get { return keys; }
        set { keys = value; }
    }
}
