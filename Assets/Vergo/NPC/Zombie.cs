﻿using UnityEngine;

[RequireComponent(typeof(NPCObjectInteraction))]
[RequireComponent(typeof(Animator))]
public class Zombie : BaseAIBehaciour
{
    [SerializeField]
    private float attackDistance;
    private float attackTimer;
    [SerializeField]
    private float attackRate = 3f;
    public Animator ani;

    public GameObject soundSource;

    void Start()
    {
        ani = this.GetComponent<Animator>();
        ani.SetBool("Walk", true);
    }

    /// <summary>
    /// This will run the zombies main decition logic 
    /// </summary>
    private void ZombieLogic()
    {
        if (base.player != null)
        {
            float distace = Vector3.Distance(this.transform.position, base.player.transform.position);
            if (distace <= attackDistance)
            {
                ani.SetBool("Attack", true);
                ani.SetBool("Walk", false);
                AttackTarget();
                base.TargetPostion = this.transform.position;
                base.inAttackRange = true;
            }
            else if (distace > attackDistance)
            {
                ani.SetBool("Attack", false);
                ani.SetBool("Walk", true);
                base.inAttackRange = false;
            }
        }
        if (attackTimer > 0)
        {
            attackTimer -= Time.deltaTime;
        }
    }

    /// <summary>
    /// Enemy attack target
    /// </summary>
    private void AttackTarget()
    {
        if (attackTimer <= 0)
        {
            base.player.GetComponent<Helath>().AdjHelath(-5);
            attackTimer = attackRate;
        }
    }

    public void PlayAttackSound()
    {
        soundSource.GetComponent<RandomZombieSounds>().PlayAttackSound();
    }

    public void StopAttackSound()
    {
        soundSource.GetComponent<RandomZombieSounds>().StopAttackSound();
    }

    public void PlayHitSound()
    {
        soundSource.GetComponent<RandomZombieSounds>().PlayHitSound();
    }

    public void StopHitSound()
    {
        soundSource.GetComponent<RandomZombieSounds>().StopHitSound();
    }

    public void PlayDeathSound()
    {
        soundSource.GetComponent<RandomZombieSounds>().PlayDeathSound();
    }

    public void StopDeathSound()
    {
        soundSource.GetComponent<RandomZombieSounds>().StopDeathSound();
    }

    public void Die()
    {
        gameObject.SetActive(false);
        Destroy(this.gameObject);
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    private void LateUpdate()
    {
        if (dead)
        {
            ani.SetBool("Attack", false);
            ani.SetBool("Walk", false);
            ani.SetBool("Die", true);

            this.enabled = false;
        }
        else
        {
            ZombieLogic();
        }
    }
}
