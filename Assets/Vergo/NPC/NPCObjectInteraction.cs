﻿using UnityEngine;
using System.Collections;

public class NPCObjectInteraction : MonoBehaviour
{
    public float _InteractDistance = 2.5f;
	
	// Update is called once per frame
	void FixedUpdate()
    {
        RaycastHit interactable;
        IInteractable t;

        Debug.DrawRay(this.transform.position + Vector3.up, this.transform.TransformDirection(Vector3.forward) * _InteractDistance, Color.red);

        if (Physics.Raycast(this.transform.position + Vector3.up, this.transform.TransformDirection(Vector3.forward), out interactable, _InteractDistance))
        {
            if ((t = interactable.collider.gameObject.GetComponentInParent<IInteractable>()) != null)
            {
                if (t is Door)
                {
                    Door d = t as Door;

                    if (!d.open)
                    {
                        t.Interact(false);
                        this.GetComponent<BaseAIBehaciour>().waitTimer = 2;
                    }
                }
            }
        }
    }
}
