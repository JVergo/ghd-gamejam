﻿using UnityEngine;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(SphereCollider))]
[RequireComponent(typeof(Helath))]
public class BaseAIBehaciour : MonoBehaviour
{
    protected NavMeshAgent _agent;
    protected bool inAttackRange;

    private Vector3 _targetPostion;
    public Vector3 TargetPostion
    {
        get { return _targetPostion; }
        set
        {
            _targetPostion = value;
            UpdateDestination(_targetPostion);
        }
    }

    [SerializeField]
    float _fealdOfViewAngle = 110f;
    private bool _playerInSight;

    private Vector3 _personalLastSighting;
    private SphereCollider _col;
    [SerializeField]
    private bool canSee = true;
    [SerializeField]
    private bool canHear = true;

    protected GameObject player;

    public bool dead;
    public float waitTimer;

    // Use this for initialization
    void Awake()
    {
        _agent = this.GetComponent<NavMeshAgent>();
        _col = this.GetComponent<SphereCollider>();
    }

    private void UpdateDestination(Vector3 target)
    {
        _agent.SetDestination(target);
    }

    public void Update()
    {
        if (!dead)
        {
            if (waitTimer <= 0)
            {
                this.GetComponent<NPCObjectInteraction>().enabled = true;
                if (!_playerInSight && TargetPostion.x == this.transform.position.x && TargetPostion.z == this.transform.position.z || TargetPostion == Vector3.zero)
                {
                    Vector3 point;
                    if (RandomPoint(transform.position, 10, out point))
                    {
                        TargetPostion = point;
                    }
                }
            }
            else
            {
                waitTimer -= Time.deltaTime;
                TargetPostion = this.transform.position;
                this.GetComponent<NPCObjectInteraction>().enabled = false;
            }
        }
        else
        {
            _agent.enabled = false;
            _col.enabled = false;
        }
    }

    void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            player = other.gameObject;
            if (canSee && !inAttackRange)
            {
                _playerInSight = false;

                Vector3 dir = other.transform.position - transform.position;
                float angle = Vector3.Angle(dir, transform.forward);

                if (angle < _fealdOfViewAngle * 0.5f)
                {
                    RaycastHit hit;

                    if (Physics.Raycast(transform.position, dir.normalized, out hit, _col.radius))
                    {
                        if (hit.collider.gameObject == player)
                        {
                            _playerInSight = true;
                            _personalLastSighting = other.transform.position;
                            TargetPostion = _personalLastSighting;
                        }
                    }
                }
            }

            if (canHear && !inAttackRange)
            {
                if (CalculatePathLength(other.transform.position) <= _col.radius + 2)
                {
                    _personalLastSighting = other.transform.position;
                    TargetPostion = _personalLastSighting;
                }
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player)
        {
            _playerInSight = false;
        }
    }

    private float CalculatePathLength(Vector3 targetPostion)
    {
        NavMeshPath path = new NavMeshPath();

        if (_agent.enabled)
        {
            _agent.CalculatePath(targetPostion, path);
        }

        Vector3[] allWayPoints = new Vector3[path.corners.Length + 2];

        allWayPoints[0] = this.transform.position;
        allWayPoints[allWayPoints.Length - 1] = targetPostion;

        for (int i = 0; i < path.corners.Length; i++)
        {
            allWayPoints[i + 1] = path.corners[i];
        }

        float pathLength = 0f;

        for (int i = 0; i < allWayPoints.Length - 1; i++)
        {
            pathLength += Vector3.Distance(allWayPoints[i], allWayPoints[i + 1]);
        }
        return pathLength;
    }

    bool RandomPoint(Vector3 center, float range, out Vector3 result)
    {
        for (int i = 0; i < 30; i++)
        {
            Vector3 randomPoint = center + Random.insideUnitSphere * range;
            NavMeshHit hit;
            if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, NavMesh.AllAreas))
            {
                result = hit.position;
                return true;
            }
        }
        result = Vector3.zero;
        return false;
    }
}
