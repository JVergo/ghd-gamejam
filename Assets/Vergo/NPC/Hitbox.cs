﻿using UnityEngine;
using System.Collections;

public class Hitbox : MonoBehaviour
{
    private Helath _helath;

    void Start()
    {
        _helath = this.GetComponentInParent<Helath>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Bullet")
        {
            _helath.AdjHelath(-1);
        }
    }
}
