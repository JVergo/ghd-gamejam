﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Helath : MonoBehaviour
{
    [SerializeField]
    float _maxHelath;
    float _curHelath;

    public Image helathBar;
    public GameObject soundSource;

    public void Start()
    {
        _curHelath = _maxHelath;
    }

    public void AdjHelath(float amount)
    {
        _curHelath += amount;

        if (_curHelath > _maxHelath)
        {
            _curHelath = _maxHelath;
        }

        if (this.gameObject.tag == "Player")
        {
            float procentHelathLeft = _curHelath / _maxHelath;
            helathBar.fillAmount = procentHelathLeft;
        }
        else
        {
            if(amount <= 0)
            {
                Zombie z;

                if ((z = this.GetComponent<Zombie>()) != null)
                {
                    z.ani.SetBool("Walk", false);
                    z.ani.SetBool("Die", false);
                    z.ani.SetBool("Attack", false);
                    z.ani.SetBool("Crawl", false);
                    z.ani.SetBool("Run", false);
                    z.ani.SetBool("Hit", true);
                    z.ani.SetBool("Hit", false);
                }
            }
        }

        if (_curHelath <= 0)
        {
            if (this.gameObject.tag != "Player")
            {
                //this.gameObject.SetActive(false);
                this.GetComponent<BaseAIBehaciour>().dead = true;
            }
            else
            {
                SceneManager.LoadScene(0);
            }
        }
    }
}
