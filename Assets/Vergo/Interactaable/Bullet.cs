﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour, IInteractable
{
    public void Interact(bool isPlayer)
    {
        if (isPlayer)
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Info>().weapon.AmmoCount++;
            Destroy(this.gameObject);
        }
    }
}
