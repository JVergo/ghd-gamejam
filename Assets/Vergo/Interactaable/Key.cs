﻿using UnityEngine;

public class Key : MonoBehaviour, IInteractable
{
    public void Interact(bool isPlayer)
    {
        if (isPlayer)
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Info>().Keys++;
            Destroy(this.gameObject);
        }
    }
}
