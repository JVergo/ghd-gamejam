﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour, IInteractable
{
    public bool open;
    public bool locked = false;
    private Animator ani;

    void Start()
    {
        ani = this.GetComponent<Animator>();

        ani.SetBool("Open", open);
    }

    public void Interact(bool isPlayer)
    {
        if (locked && isPlayer)
        {
            Player_Info pi = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Info>();
            if (pi.Keys > 0)
            {
                pi.Keys--;
                locked = false;
            }
        }
        if (!locked)
        {
            open = !open;

            ani.SetBool("Open", open);
        }
    }
}
