using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour
{
	
	public float speed;
	
	// Update is called once per frame
	void Update ()
    {
        transform.Rotate(new Vector3(speed * Time.deltaTime, 0, 0));
	}
}
