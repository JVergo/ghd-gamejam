using UnityEngine;
using System.Collections;

public class CycleCookiesTimer : MonoBehaviour
{
	public float interval = 5.0f;
	public Cubemap[] cookies;
    public Color[] colors;
	
	private float _interval;
	private int i;

    private System.Random rand;
	
	// Use this for initialization
	void Start ()
    {
		gameObject.GetComponent<Light>().cookie = cookies[0];
        gameObject.GetComponent<Light>().color = colors[0];
        _interval = interval;

        rand = new System.Random();
	}
	
	// Update is called once per frame
	void Update ()
    {
        _interval -= Time.deltaTime;

        if (_interval <= 0.0f)
        {
            i++;

            if (i >= cookies.Length)
            {
                i = 0;
            }

            gameObject.GetComponent<Light>().cookie = cookies[i];
            int newColor = rand.Next(0, colors.Length);
            gameObject.GetComponent<Light>().color = colors[newColor];

            _interval = interval;
        }
    }
}
