﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DanceFloor : MonoBehaviour
{
    public GameObject tilePrefab;

    public List<Material> tileMaterials;

    public Material deactiveMaterial;

    public int tilesColumns = 5;
    public int tilesRows = 5;

    [Range(0, 1)]
    public float tileScale = 1.0f;

    private List<GameObject> tiles;
    private System.Random rand;
    private bool dancing;

    public void BeginDance()
    {
        if (!dancing)
        {
            RandomizeTiles();

            foreach (GameObject obj in tiles)
            {
                foreach (Transform child in obj.transform)
                {
                    child.gameObject.SetActive(true);
                }
            }

            Timer.Register(1.5f, () => RandomizeTiles(), isLooped: true);

            dancing = true;
        }
    }

    // Awake is called when the script instance is being loaded
    public void Awake()
    {

        dancing = false;

        if(tiles == null)
        {
            tiles = new List<GameObject>();
        }

        rand = new System.Random();

        for (int i = 0; i < (tilesRows * tilesColumns); i++)
        {
            // Instantiate floor tiles
            GameObject tile = Instantiate(tilePrefab);
            tile.transform.parent = gameObject.transform;
            tile.transform.localScale = new Vector3(0.1f * tileScale, 0.1f * tileScale, 0.1f * tileScale);
            tiles.Add(tile);
        }

        int count = 0;

        for (int i = 0; i < tilesRows; i++)
        {
            for (int j = 0; j < tilesColumns; j++)
            {
                tiles[count].transform.localPosition = new Vector3(i * tileScale, 0, j * tileScale);
                count++;
            }
        }

        foreach (GameObject obj in tiles)
        {
            foreach (Transform child in obj.transform)
            {
                child.gameObject.SetActive(false);
            }

            obj.GetComponent<Renderer>().material = deactiveMaterial;
        }
    }

    public void RandomizeTiles()
    {
        foreach (GameObject obj in tiles)
        {
            int color = rand.Next(0, 6);
            obj.GetComponent<Renderer>().material = tileMaterials[color];

            foreach (Transform child in obj.transform)
            {
                child.gameObject.GetComponent<Light>().color = tileMaterials[color].GetColor("_GlowColor");
            }
        }
    }
}
