﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class RandomZombieSounds : MonoBehaviour
{
    public AudioClip[] sounds;

    public AudioClip attackSound;
    public AudioClip deathSound;
    public AudioClip hitSound;

    private bool playAudio;
    private AudioSource audioSource;
    private Timer zombieMoans;

    public void Awake()
    {
        playAudio = false;
        audioSource = GetComponent<AudioSource>();

        zombieMoans = Timer.Register(0.8f, () => PlaySound(), isLooped: true);

    }

    public void Update()
    {
        //Timer.Register(Random.Range(1, 10), () => PlaySound());
    }

    public void PlaySound()
    {
        if (!audioSource.isPlaying && playAudio)
        {
            audioSource.PlayOneShot(sounds[Random.Range(0, sounds.Length)]);
        }

        playAudio = !playAudio;
    }

    public void PlaySound(AudioClip clip)
    {
        if (!audioSource.isPlaying && playAudio)
        {
            audioSource.PlayOneShot(clip);
        }

        playAudio = !playAudio;
    }

    public void PlayAttackSound()
    {
        zombieMoans.Pause();
        audioSource.Stop();
        playAudio = true;
        PlaySound(attackSound);
        zombieMoans.Resume();
    }

    public void StopAttackSound()
    {
        audioSource.Stop();
    }

    public void PlayHitSound()
    {
        zombieMoans.Pause();
        audioSource.Stop();
        playAudio = true;
        PlaySound(hitSound);
        zombieMoans.Resume();
    }

    public void StopHitSound()
    {
        audioSource.Stop();
    }

    public void PlayDeathSound()
    {
        zombieMoans.Pause();
        audioSource.Stop();
        playAudio = true;
        PlaySound(deathSound);
    }

    public void StopDeathSound()
    {
        audioSource.Stop();
    }
}
