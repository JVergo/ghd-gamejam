﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class credits : MonoBehaviour {

    private RectTransform cred;

	// Use this for initialization
	void Start () {
        cred = this.gameObject.GetComponent<RectTransform>();
	}
	
	// Update is called once per frame
	void Update () {
        cred.position += Vector3.up;
	}
}
