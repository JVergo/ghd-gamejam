﻿using UnityEngine;
using System.Collections;

public class Enddance : MonoBehaviour
{

    private Animator _animator;

    public RuntimeAnimatorController defaultmode;
    public RuntimeAnimatorController dance;

    public GameObject goHere;

    private float speed = 0.5f;
    private Vector3 _lastPos;

    private bool dancing;

    // Use this for initialization
    void Start()
    {
        _animator = this.gameObject.GetComponent<Animator>();

        _animator.runtimeAnimatorController = defaultmode;

        _animator.SetBool("Walk", true);
    }



    // Update is called once per frame
    void Update()
    {
        if (!dancing)
        {
            //Set the current position to check next movement
            Vector3 curPos = this.transform.position;

            if (curPos == _lastPos)
            {
                dancing = true;
                _animator.runtimeAnimatorController = dance;

                _animator.SetBool("Dance", true);
            }


            //Set the current position to be last position for next iteration
            _lastPos = curPos;

            this.gameObject.transform.position = Vector3.MoveTowards(transform.position, goHere.transform.position, speed * Time.deltaTime);
        }

    }
}
