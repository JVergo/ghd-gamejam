﻿using UnityEngine;
using System.Collections;

public class discolight : MonoBehaviour {

    public ParticleSystem core;
    public Light light;

    private float color;
    private bool calculat;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {

            core.startColor = Color.Lerp(Color.red, Color.green, Mathf.PingPong(Time.time, 2f));
        light.color = Color.Lerp(Color.red, Color.green, Mathf.PingPong(Time.time, 2f));
    }
}
