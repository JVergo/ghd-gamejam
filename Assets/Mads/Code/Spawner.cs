﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawner : MonoBehaviour
{
    public static Spawner instant;              //Instantiate singelton

    [SerializeField]
    public GameObject[] enemyPrefabs;           //List of all enemies the spawner need 
    public int count;                           //Number of times to spawn each enemy
    public Vector3[] spawnLocations;            //List of all enemies spawn locations

    public List<GameObject> recycledEnemies;    //Object pool containing all spawned objects

    private float _spawnTimer;                  //Time until next spawn
    public float spawnTimeMin;                  //Minimum timer interval for spawning 
    public float spawnTimeMax;                  //Maximum timer interval for spawning 

    private int _tempLocation;                  //Random spawning location

    /// <summary>
    /// Use this for initialization before start
    /// </summary>
    private void Awake()
    {
        if (instant == null)
        {
            instant = this;
        }
    }

    /// <summary>
    /// Use this for initialization
    /// </summary>
    private void Start()
    {
        recycledEnemies = new List<GameObject>();
        FindAllSpawnPoints();
        InstantiateAllEnemies();
    }

    /// <summary>
    /// Adds all enemies to the enmy pool if objPool is true
    /// </summary>
    private void InstantiateAllEnemies()
    {
        for (int i = 0; i < enemyPrefabs.Length; i++)
        {
            for (int e = 0; e < count; e++)
            {
                GameObject newEnemy = Instantiate(enemyPrefabs[i]) as GameObject;
                newEnemy.transform.position = new Vector3(1000, 1000, 1000);
                newEnemy.transform.SetParent(this.gameObject.transform);
                recycledEnemies.Add(newEnemy);
            }
        }
    }

    /// <summary>
    /// Calculates the time until next spawn
    /// </summary>
    private void SpawnTimer()
    {
        if (_spawnTimer > 0)
        {
            _spawnTimer -= Time.deltaTime;
        }
        else
        {
            _spawnTimer = Random.Range(spawnTimeMin, spawnTimeMax);
            _tempLocation = Random.Range(0, spawnLocations.Length); //Random spawn location

            SpawnNewEnemyFromPool();
        }
    }

    /// <summary>
    /// Spawn a new enemy with object pooling at random location
    /// </summary>
    private void SpawnNewEnemyFromPool()
    {
        bool spawned = false;

        //Loop list of enemies to find an inactive enemy
        for (int i = 0; i < recycledEnemies.Count; i++)
        {
            if (!recycledEnemies[i].activeInHierarchy)
            {
                recycledEnemies[i].transform.position = spawnLocations[_tempLocation];
                recycledEnemies[i].SetActive(true);
                spawned = true;
                break;
            }
        }

        //if there was no enemy to spawn
        if (!spawned)
        {
            SpawnNewEnemy();
        }
    }

    /// <summary>
    /// Spawn new enemy without object pooling at random location
    /// </summary>
    private void SpawnNewEnemy()
    {
        GameObject newEnemy = Instantiate(enemyPrefabs[Random.Range(0, enemyPrefabs.Length)]) as GameObject;
        newEnemy.transform.position = spawnLocations[_tempLocation];
        newEnemy.transform.SetParent(this.gameObject.transform);
        newEnemy.SetActive(true);
        recycledEnemies.Add(newEnemy);
    }

    /// <summary>
    /// Find all spawnpoints and add the position to the spawnPositions list
    /// </summary>
    private void FindAllSpawnPoints()
    {
        //Find all spawnloacations
        GameObject[] go = GameObject.FindGameObjectsWithTag("Spawner");

        //instantiate array
        spawnLocations = new Vector3[go.Length];

        //Add spawnlocations vector3 positions to spawnPositions
        for (int i = 0; i < go.Length; i++)
        {
            spawnLocations[i] = go[i].transform.position;
        }
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    private void LateUpdate()
    {
        SpawnTimer();
    }
}
